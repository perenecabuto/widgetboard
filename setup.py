# -*- coding: utf-8 -*-

import os
from pip.req import parse_requirements
from setuptools import setup

with open('README.md') as f:
    readme = f.read()

requirements_path = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "requirements.txt")

requirements = [str(ir.req) for ir in parse_requirements(requirements_path)]

setup(
    name='widgetboard',
    version='0.1.2',
    description='',
    long_description=readme,
    author='Felipe Ramos Ferreira',
    author_email='perenecabuto@gmail.com',
    license='BSD',
    url='...',
    scripts=['bin/widgetboard_web', 'bin/widgetboard_taskserver'],
    include_package_data=True,
    install_requires=requirements,
    packages=['widgetboard'],
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: Linux',
        'Programming Language :: Python',
    ],
    zip_safe=False,
)


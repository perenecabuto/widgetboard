# -*- coding: utf-8 -*-

import json

import gevent
from flask import Response, render_template, request

from .environment import app, events


@app.route("/")
def main():
    return render_template('main.html')


@app.route("/calltask/<path:task_name>")
def calltask(task_name):
    events.send_event('calltask', task_name)

    return "true"


@app.route('/events', methods=['GET'])
def get_events():
    if request.headers.get('Accept') == 'text/event-stream':
        return Response(stream_events(), mimetype='text/event-stream')

    return Response(events.to_json(), mimetype='application/json')


@app.route('/events', methods=['POST'])
def set_event():
    if request.data:
        message = json.loads(request.data)

        if message.get('id') and 'value' in message:
            events.send_event(message['id'], message['value'])
            return "true"

    return "false"


def stream_events():
    last_message = ''

    while True:
        gevent.sleep(0.5)
        curr_message = events.to_json()

        if curr_message != last_message:
            last_message = curr_message
            yield 'data: %s\n\n' % curr_message

# -*- coding: utf-8 -*-

import os

import jinja2
from werkzeug.exceptions import NotFound

from flask import Flask
from flask.helpers import send_from_directory


class DashFlask(Flask):

    def __init__(self, *args, **kwargs):
        local_template_folder = kwargs.pop('local_template_folder', '')
        local_static_folder = kwargs.pop('local_static_folder', '')

        super(DashFlask, self).__init__(*args, **kwargs)

        self.default_template_folder = os.path.join(os.path.dirname(__file__), self.template_folder)
        self.default_static_folder = self.static_folder
        self.template_folder = local_template_folder or self.default_template_folder
        self.static_folder = local_static_folder or self.default_static_folder

        self.jinja_loader = jinja2.ChoiceLoader([
            self.jinja_loader, jinja2.FileSystemLoader(self.default_template_folder)
        ])

    def send_static_file(self, filename, *args, **kwargs):
        try:
            return super(DashFlask, self).send_static_file(filename, *args, **kwargs)
        except NotFound:
            return send_from_directory(self.default_static_folder, filename)

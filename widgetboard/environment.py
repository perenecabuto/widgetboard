# -*- coding: utf-8 -*-

import os

from flask.ext.assets import Environment


from .events import EventBus
from .task_manager import TaskScheduler
from . import utils
from .dashboard_flask import DashFlask

# Paths and settings
dashboard_path = os.environ.setdefault('WIDGETBOARD_PATH', os.getcwd())
config_file_name = os.environ.setdefault('WIDGETBOARD_CONFIG_FILE', 'config.py')
widget_settings = os.environ.setdefault(
    'WIDGETBOARD_SETTINGS',
    utils.discover_config_file(dashboard_path, config_file_name) or ''
)

utils.add_to_pythonpath(dashboard_path)


# Flask
app = DashFlask(
    __name__,
    local_template_folder=os.path.join(dashboard_path, 'templates'),
    local_static_folder=os.path.join(dashboard_path, 'static')
)

app.config.from_object('widgetboard.settings')

if widget_settings:
    app.config.from_envvar('WIDGETBOARD_SETTINGS')

assets = Environment(app)


# Events and scheduler
events = EventBus(app.config.get('BUS_FILE_PATH'))
sched = TaskScheduler(app.config.get('APS_CONFIG'))

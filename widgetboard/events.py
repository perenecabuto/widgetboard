# -*- coding: utf-8 -*-

import json
from datetime import datetime

from sqlitedict import SqliteDict


class EventBus(object):

    def __init__(self, bus_file_path):
        self._events = SqliteDict(bus_file_path, autocommit=True)
        self._bus_file_path = bus_file_path

    @property
    def events(self):
        return self._events.values()

    def get(self, event_id):
        return self._events.get(event_id)

    def remove(self, event_id):
        value = self._events.get(event_id)

        if value:
            del self._events[event_id]

        return value

    def to_json(self):
        return json.dumps(self.events)

    def send_event(self, event_id, event_data):
        self._events[event_id] = self.build_event(event_id, event_data)

    def build_event(self, event_id, event_data):
        return {
            'id': event_id,
            'value': event_data,
            'last-update': datetime.now().isoformat()
        }

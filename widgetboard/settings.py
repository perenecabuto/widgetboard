# -*- coding: utf-8 -*-

DEBUG = True
ASSETS_DEBUG = True

BUS_FILE_PATH = '/tmp/widgetboard-event-bus.db'

APS_CONFIG = {
    'apscheduler.misfire_grace_time': 3,
    'apscheduler.daemonic': True,
    'apscheduler.standalone': True,
    'apscheduler.threadpool.max_threads': 2,
    'apscheduler.threadpool.core_threads': 1,
    'apscheduler.jobstores.file.class': 'apscheduler.jobstores.shelve_store:RAMJobStore',
    #'apscheduler.jobstores.file.class': 'apscheduler.jobstores.shelve_store:ShelveJobStore',
    #'apscheduler.jobstores.file.path': '/tmp/lukaboard-scheduler.db',
}

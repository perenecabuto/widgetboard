var widgetboard = angular.module('widgetboard', ['widgets']);

widgetboard

.controller('Dashboard', ['$scope', 'EventBus',
function($scope, EventBus) {
    EventBus.connect(location.href + '/events');
}])

.service('EventBus', ['$rootScope', '$http', '$timeout',
function($rootScope, $http, $timeout) {

    this.register = function(id, callback) {
        $rootScope.$on('widgetboard-' + id, callback);
    };

    this.connect = function(url) {
        var supportEventSource = window.EventSource ? true : false;
        var onerror = function() {
            //if (confirm("Connection error. Try again?"))
            setTimeout(function() { connect(url, message_callback) }, 5000);
        }

        if (supportEventSource) {
            var es = new EventSource(url);
            es.onerror = onerror;
            es.onmessage = function(e) {
                handleEvents(JSON.parse(e.data));
            };
        } else {
            $http({method: 'GET', url: url})
            .error(onerror)
            .success(function(data) {
                handleEvents(data);
                $timeout(function() { connect(url) }, 1000);
            });
        }
    };

    function handleEvents(events) {
        for (var i = 0; i < events.length; i++) {
            var data = events[i];
            $rootScope.$emit('widgetboard-' + data.id, data);
        }
    };

}])

.directive('listenTo', ['EventBus', '$compile',
function(EventBus, $compile) {
    return {
        scope: true,
        restrict: 'A',
        link: function(scope, element, attr) {
            scope.eventId = attr.listenTo;

            element.append($compile('<small class="text-info pull-right">{{ eventId }} - {{ lastUpdate }}</small>')(scope));

            EventBus.register(scope.eventId, function(e, data) {
                scope.$apply(function() {
                    _.each(data.value, function(value, key) {
                        scope[key.replace("-", "_")] = value;
                    })

                    scope.lastUpdate = data['last-update'];
                });
            });
        }
    }
}])

.directive('touchEvent', ['$http',
function($http) {
    return {
        restrict: 'A',
        link: function(scope, element, attr) {
            var taskName = attr.touchEvent;

            element.on('click', function(e) {
                e.preventDefault();

                $http.post('/events', {id: taskName, value: {}})
                .success(function(data) {
                    scope.taskData = data;
                });

                return false;
            });
        }
    }
}]);
;


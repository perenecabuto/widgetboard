var widgets = angular.module('widgets', []);

widgets

.directive('statusBar', function() {
    return {
        restrict: 'E',
        template: '' +
        '<div>' +
            '<div class="progress pull-left" style="width: 60%">' +
                '<div class="widget-status-bar progress-bar {{ status_class }}" style="width: {{ percent }}%;">{{ percent }}% ({{ value }})</div>' +
            '</div>' +
            '<div class="widget-status label {{ label_class }} pull-right" style="display: inline-block; width: 38%">{{ status }}</div>' +
        '</div>',

        link: function(scope) {
            scope.percent = 0;
            scope.value = 0;
            scope.status = '--';

            scope.$watch('percent', function(percent) {
                scope.status_class =
                    (percent >= 75) ? 'progress-bar-info' :
                    (percent >= 61) ? 'progress-bar-warning' :
                    'progress-bar-danger';
            });

            scope.$watch('status', function(status) {
                scope.status = scope.status || '--';
                scope.label_class =
                    (status === 'IDLE') ? 'label-success' :
                    (status === 'WORKING') ? 'label-warning' :
                    'label-danger';
            });

        }
    }
})

.directive('keyValue', function() {
    return {
        restrict: 'E',
        template: '' +
        '<ul class="list-group">' +
            '<li class="list-group-item" ng-repeat="item in items">' +
                '<span class="label pull-right {{ item.status_class }}" style="min-width: 45px">{{ item.value }}</span>' +
                '<span>{{ item.label }} {{ item.url }}</span>' +
            '</li>' +
        '</ul>',

        link: function(scope) {
            scope.items = [{label: '--', value: '--'}];

            scope.$watchCollection('items', function() {
                for (var i = 0; i < scope.items.length; i++) {
                    var item = scope.items[i];
                    item.status_class = 'label-' + (item.status || 'info');
                }
            });
        }
    };
})

.directive('randomImage', ['$timeout', function($timeout) {
    var errorImg = "static/img/black-pixel.jpg";

    return {
        restrict: 'E',
        scope: {},
        template: '<img ng-src="{{ imgSrc }}" style="width: 100%" />',
        link: function(scope, element, attr) {
            if (!attr.src) return false;

            var img = element.find('img');

            if (attr.width) img.css({width: attr.width});
            if (attr.height) img.css({height: attr.height});

            img.bind('error', function() {
               angular.element(this).attr('src', attr.brokenImg || location.href + '/static/img/black-pixel.jpg');
            });

            var autoUpdate = function() {
                var separator = attr.src.match(/\?/) ? "&" : "?"
                scope.imgSrc = attr.src + separator + Math.random();
                $timeout(autoUpdate, 1000);
            };

            autoUpdate();
        }
    };
}])

.directive('actionButton', ['$http',
function($http) {
    return {
        restrict: 'E',
        scope: {},
        template: '<button type="button" class="btn btn-lg {{ extraClass }}" style="width: 100%; margin-bottom: 2px">{{ label }}</button>',
        link: function(scope, element, attr) {
            scope.extraClass = attr.extraClass;
            scope.label = attr.label;

            var url = attr.action.match(/:\/\//) ? attr.action : location.href + "/" + attr.action;

            element.on('click', function(e) {
                e.preventDefault();
                $http({'url': url, 'method': attr.method || 'JSONP'})
                .success(function(data) {
                    scope.label = data;
                });
                return false;
            });
        }
    };
}])
;


/*
var Sparkline = Backbone.Model.extend({
    defaults: {
        values: []
    }
});

var SparklineView = Backbone.View.extend({
    template: '<span class="dynamic sparkline-widget">Loading..</span>',
    widget: null,

    sparklineOpts: {
        height: 20,
        width: '100%',
        valueSpots: {'0': 'transparent', ':600': 'red'},
        lineColor: '#ccc',
        fillColor: '#eee'
    },

    initialize: function() {
        this.model.on('change', this.update, this);
    },

    update: function() {
        var values = this.model.get('values');
        this.widget.sparkline(values, this.sparklineOpts);
    },

    render: function() {
        this.$el.html(this.template);
        this.widget = this.$el.find('.sparkline-widget');
        this.update();
    }
});

*/

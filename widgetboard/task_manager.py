# -*- coding: utf-8 -*-

import random
import logging
from multiprocessing import Process

from apscheduler.scheduler import Scheduler
from apscheduler.util import get_callable_name


class TaskScheduler(Scheduler):

    widgetboard_tasks = {}

    # Overriding
    def start(self):
        self.load_task_modules()
        super(TaskScheduler, self).start()

    # Overriding
    def add_job(self, trigger, func, *args, **kwargs):
        task_name = get_callable_name(func)

        while task_name in self.widgetboard_tasks:
            task_name = "{0}-{1}".format(task_name, random.randint(0, 10000))
            logging.warn("!Skipping: Duplicated task name. Traying to register task as " + task_name)

        self.widgetboard_tasks[task_name] = func
        super(TaskScheduler, self).add_job(trigger, func, *args, **kwargs)

    def find_and_run(self, task_name):
        task = self.widgetboard_tasks.get(task_name)

        if task:
            Process(target=task).start()

    def load_task_modules(self, base_modules=['tasks']):
        __import__('widgetboard.tasks')

        for module in base_modules:
            __import__(module)


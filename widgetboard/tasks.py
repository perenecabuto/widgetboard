# -*- coding: utf-8 -*-

import logging
from widgetboard.environment import events, sched


@sched.interval_schedule(seconds=1)
def calltask():
    event_message = events.remove('calltask')

    if event_message:
        task_name = event_message.get('value')
        logging.info("Running task: %s" % task_name)
        sched.find_and_run(task_name)


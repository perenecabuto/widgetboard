# -*- coding: utf-8 -*-

import os
import sys


def add_to_pythonpath(path):
    if path not in sys.path:
        sys.path.append(path)


def discover_config_file(base_dir, config_file_name):
    base_dir = os.path.realpath(base_dir)
    splited_dir = base_dir.split(os.path.sep)

    for idx in range(len(splited_dir)):
        splited_dir = splited_dir[0:len(splited_dir) - idx]
        base_dir = os.path.join(os.path.sep, *splited_dir)
        config_file_path = os.path.join(base_dir, config_file_name)

        if os.path.exists(config_file_path):
            return config_file_path
